'use strict'

const test = require('ava')
const nock = require('nock')
const developgram = require('../')
const fixtures = require('./fixtures')

const options = {
  endpoints: {
    pictures: 'http://developgram.test/picture',
    users: 'http://developgram.test/user',
    auth: 'http://developgram.test/auth'
  }
}

test.beforeEach(t => {
  t.context.client = developgram.createClient(options)
})

test('cliente', t => {
  const client = t.context.client
  t.is(typeof client.getPicture, 'function', 'getPicture es una funcion')
  t.is(typeof client.savePicture, 'function', 'savePicture es una funcion')
  t.is(typeof client.likePicture, 'function', 'likePicture es una funcion')
  t.is(typeof client.listPictures, 'function', 'listPictures es una funcion')
  t.is(typeof client.listPicturesByTag, 'function', 'listPicturesByTag es una funcion')
  t.is(typeof client.saveUser, 'function', 'saveUser es una funcion')
  t.is(typeof client.getUser, 'function', 'getUser es una funcion')
  t.is(typeof client.auth, 'function', 'auth es una funcion')
})

test('getPicture', async t => {
  const client = t.context.client
  const image = fixtures.getImage()

  nock(options.endpoints.pictures)
    .get(`/${image.publicId}`)
    .reply(200, image)
  const result = await client.getPicture(image.publicId)
  t.deepEqual(image, result)
})

test('savePicture', async t => {
  const client = t.context.client
  const token = '111-111-111'
  const image = fixtures.getImage()
  const newImage = {
    src: image.url,
    description: image.description
  }
  nock(options.endpoints.pictures, {
    // headers a enviar en la peticion http
    reqheaders: {
      'authorization': `Bearer ${token}` // eslint-disable-line quote-props
    }
  })
    // .filteringPath(() => '')
    .post('/', newImage) // ruta, objeto a enviar
    .reply(201, image) // status, respuesta
    // .log(console.log)
  const result = await client.savePicture(newImage, token)
  t.deepEqual(result, image)
})

test('likePicture', async t => {
  const client = t.context.client
  const image = fixtures.getImage()
  image.liked = true
  image.likes = 1

  nock(options.endpoints.pictures)
    .post(`/${image.publicId}/like`)
    .reply(200, image)
  const result = await client.likePicture(image.publicId)
  t.deepEqual(image, result)
})

test('listPicture', async t => {
  const client = t.context.client
  const images = fixtures.getImages(3)

  nock(options.endpoints.pictures)
    .get('/list')
    .reply(200, images)
  const result = await client.listPictures()
  t.deepEqual(images, result)
})

test('listPictureByTag', async t => {
  const client = t.context.client
  const images = fixtures.getImages(3)
  const tag = 'developgram'

  nock(options.endpoints.pictures)
    .get(`/tag/${tag}`)
    .reply(200, images)
  const result = await client.listPicturesByTag(tag)
  t.deepEqual(images, result)
})

test('saveUser', async t => {
  // video 3 --  16:57
  const client = t.context.client
  const user = fixtures.getUser()
  const newUser = {
    username: user.username,
    name: user.name,
    id: user.id,
    createdAt: user.createdAt
    // email: 'user_test@developgram.test',
    // password: 'test_pass'
  }
  nock(options.endpoints.users)
    .post('/', newUser)
    .reply(201, user)
  const result = await client.saveUser(newUser)
  t.deepEqual(result, newUser)
})

test('getUser', async t => {
  const client = t.context.client
  const user = fixtures.getUser()
  nock(options.endpoints.users)
    .get(`/${user.username}`)
    .reply(200, user)
    // .log(console.log)

  const result = await client.getUser(user)
  t.deepEqual(result, user)
})

test('auth', async t => {
  const client = t.context.client
  const credentianls = {
    username: 'flavio',
    password: 'pass_test'
  }
  const token = '111-111-111'
  nock(options.endpoints.auth)
    .post('/', credentianls)
    .reply(200, token)
    .log(console.log)

  const result = await client.auth(credentianls.username, credentianls.password)
  t.deepEqual(result, token)
})
