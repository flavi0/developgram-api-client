'use strict'

const request = require('request-promise')
const Promise = require('bluebird')

class Client {
  constructor (options) {
    this.options = options || {
      endpoints: {
        pictures: 'http://api.developgram.com/picture',
        users: 'http://api.developgram.com/user',
        auth: 'http://api.developgram.com/auth'
      }
    }
  }

  getPicture (id, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.pictures}/${id}`,
      json: true
    }
    return Promise.resolve(request(options)).asCallback(callback)
  }

  savePicture (picture, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.pictures}/`,
      body: picture,
      headers: {
        'authorization': `Bearer ${token}` // eslint-disable-line quote-props
      },
      json: true
    }
    const result = Promise.resolve(request(options)).asCallback(callback)
    return result
  }

  likePicture (id, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.pictures}/${id}/like`,
      json: true
    }
    return Promise.resolve(request(options)).asCallback(callback)
  }

  listPictures (callback) {
    console.log(`client-listPictures: ${this.options.endpoints.pictures}/list`)
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.pictures}/list`,
      json: true
    }
    return Promise.resolve(request(options)).asCallback(callback)
  }

  listPicturesByTag (tag, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.pictures}/tag/${tag}`,
      json: true
    }
    return Promise.resolve(request(options)).asCallback(callback)
  }

  saveUser (user, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/`,
      body: user,
      json: true
    }
    return Promise.resolve(request(options)).asCallback(callback)
  }

  getUser (username, callback) {
    console.log('client-getUser-user-->', username)
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/${username}`,
      json: true
    }
    console.log('client-getUser-->', options)
    return Promise.resolve(request(options)).asCallback(callback)
  }

  auth (username, password, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.auth}/`,
      body: {
        username,
        password
      },
      json: true
    }
    console.log('auth--> ', options)
    return Promise.resolve(request(options)).asCallback(callback)
  }
}

module.exports = Client
